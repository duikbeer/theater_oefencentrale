import re

cleaned = ''
# raw_file = 'pierrot_pdf.txt'
raw_file = 'lear_pdf.txt'
# out_file = 'pierrot.txt'
out_file = 'lear.txt'

with open(raw_file, 'r') as f:
    lines = f.read()
    pattern = r'-*\n(?![A-ZÉ]{3,})|(___*)|Al Dente - Koning Lear in een kwartier '

    # pattern = r'-*\n(?![A-ZÉ]{3,})|(___*)|(Al Dente – Mijn vriend Pierrot )'
    cleaned = re.sub(pattern, '', lines)

with open(out_file, 'w+') as txt:
    txt.write(cleaned)

# print(cleaned)
