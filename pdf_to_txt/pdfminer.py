import os.path 
import sys
from pdfminer.high_level import extract_text

file = sys.argv[1]

if os.path.isfile(file):
    file_name = os.path.basename(file).split('.')[0] + '.txt'

    with open(file_name, 'w') as new_file:
         new_file.write(extract_text(file))

else:
    print('pdf bestand nodig.')
    exit()

