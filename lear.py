#!/bin/python3
import os
from termcolor import colored
# Je hebt python3-pip nodig en termcolor library via pip 
# https://pypi.org/project/termcolor/

def clean(line):
    return line.strip("\n").strip()

def print_hoofdstukken():
    print("########################\n")

    i = 0
    for k in d:
      print(i, k)
      i += 1

    print("\n########################")

def kies_hoofdstuk(len):
    keuze = input('\nKies een hoofdstuk: (q voor afsluiten en m voor dit menu)\n')

    if keuze.lower() == 'q':
        afsluiten()

    if keuze.lower() == 'm':
        print_hoofdstukken()
        return -3

    elif keuze.isdigit():
        keuze = int(keuze)

        if keuze >= len:
            print('Dit hoofdstuk bestaat niet.')
            return -1

        else:
            return keuze

    else:
        print('Toets een getal in.')
        return -2

def afsluiten():
    print('Afsluiten.')
    exit()


d = {}

# rol = 'CROUÏA-BEY(8)'
# rol = 'VOUSSOIS(9)'
rol = 'EDGAR'

# Load script into dict
with open('lear.txt', 'r') as my_file:
    # script moet beginnen met eerste hoofdstuk indicator '//'
    hoofdstuk = my_file.readline(0)
    lines = my_file.readlines()
    previous_line = ''
    i = 0

    for line in lines:
        if line.startswith("\n"):
            continue
        
        if line.startswith("//"):
            hoofdstuk = clean(line.split("//")[1])
            d[hoofdstuk] = []

        if line.startswith(rol):
            cue = clean(previous_line)
            txt = clean(line)
            d[hoofdstuk].append({
                'cue': cue,
                'txt': txt,
            })
        previous_line = line
        i += 1



print("\n\nOEFENCENTRALE v1.2\n")

# Start de repetitie:
while True:
    print_hoofdstukken()

    h_list = list(d.keys())
    menu = -3

    # het hoofdstukmenu blijft aan als er geen geldige keuze wordt gemaakt
    while menu < 0:
        menu = kies_hoofdstuk(len(h_list))

    cues = d[h_list[menu]]
    i = 0;

    print(h_list[menu] +
          colored('\nDruk ENTER om het antwoord te zien,\nof SPATIE om een cue terug te gaan.\n', 'grey'))

    while i in range(len(cues)):
        # print de cue
        print(colored(cues[i]['cue'] + '\n', 'red'))
        keuze = input('>>')
        # print het antwoord
        print(colored(cues[i]['txt'] + '\n', 'green'))

        if keuze == '':
            i += 1

        elif keuze == ' ':
            i -= 1
            os.system('clear')

        elif keuze == 'm':
            break

        elif keuze == 'q':
            afsluiten()

        else:
            break
