## Theater oefencentrale

### Notes
Een paar dingen moeten nog wat opgeruimd worden.  
Alleen de tekst van Pierrot heb ik gecheckt. De andere rollen zouden hier en daar nog cues kunnen missen, door een foutje in cleanup.py.

### To do
Lear.py moet nog worden geupdate.  
Pierrot.py zoekt nu op een absolute path naar pierrot.txt, maar zonder absolute path kan ik geen snelkoppeling maken in mijn home folder.  
De help files die het originele pdf script omzetten naar txt files werken nu even niet, weet niet waarom.  
cleanup.py bevat de regex commands om de raw txt files te ontdoen van extra enters en andere extra tekens van het originele script. Er ontbreekt nog een stukje regex om een zin die eindigt met het '...' teken te verwijderen.  
De scripts wil ik nog zo maken dat ze voor toekomstige stukken makkelijk te gebruiken zijn.  
Het zou leuk zijn om in command line arguments de te oefenen rol te kunnen selecteren.

### Dependencies
Voor gekleurde tekst in de terminal gebruik ik [termcolor](https://pypi.org/project/termcolor/), te installeren als je [PyPI](https://pypi.org/) ook hebt geinstalleerd.
